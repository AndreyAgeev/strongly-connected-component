# -*- coding: utf-8 -*-
"""
@author: Андрей
"""
from collections import defaultdict


order = []
component = []


def create_graph_with_inverse(file):
    graph = defaultdict(list)
    inv_graph = defaultdict(list)
    with open(file) as f:
        for line in f:
            vertex_from, vertex_to = map(int, line.split())
            graph[vertex_from].append(vertex_to)
            inv_graph[vertex_to].append(vertex_from)
    return graph, inv_graph


def dfs_graph(vertex, visited, graph):
    visited[vertex] = True
    for v in graph[vertex]:
        if not visited[v]:
            dfs_graph(v, visited, graph)
    order.append(vertex)


def dfs_inv_graph(vertex, visited, graph):
    visited[vertex] = True
    component.append(vertex)
    for v in graph[vertex]:
        if not visited[v]:
            dfs_inv_graph(v, visited, graph)


def find_component(graph, inv_graph):
    components = []
    global order,  component
    visited = [False] * (max(graph)+1)
    for v in graph:
        if not visited[v]:
            dfs_graph(v, visited, graph)
    visited = [False] * (max(graph)+1)
    order = order[::-1]
    for v in order:
        if not visited[v]:
            dfs_inv_graph(v, visited, inv_graph)
            components.append(component)
            component = []
    return components
