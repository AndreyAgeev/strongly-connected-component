# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 14:43:01 2019

@author: Андрей
"""

import unittest
import task4
class TestMethods(unittest.TestCase):
    def test_read_file(self):
        self.assertEqual(task4.create_graph_with_inverse("file.txt"), ({0: [1], 1: [2, 3], 2: [0, 3], 3: [4], 4: [3], 5: [4, 6], 6: [5, 7], 7: [6, 4]},
                          {1: [0], 2: [1], 3: [1, 2, 4], 0: [2], 4: [3, 5, 7], 6: [5, 7], 5: [6], 7: [6]}))


    def test_file_with_3_component(self):
        graph, inv_graph = task4.create_graph_with_inverse("file.txt")
        self.assertEqual(task4.find_component(graph, inv_graph), [[5, 6, 7], [0, 2, 1], [3, 4]])

      
    def test_file_with_2_component(self):
        graph, inv_graph = task4.create_graph_with_inverse("file_1.txt")
        self.assertEqual(task4.find_component(graph, inv_graph), [[6, 5], [0, 1, 2, 3]])

if __name__ == '__main__':
    unittest.main()